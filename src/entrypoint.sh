#!/bin/sh

# If the PORT environment variable is not set, use the default port (8080)
PORT=${PORT:-8080}

# Start the Cowsay server
npm start
